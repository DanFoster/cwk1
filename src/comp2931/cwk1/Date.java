// Class for COMP2931 Coursework 1 - By Daniel Foster (sc16df)

package comp2931.cwk1;


/**
 * Simple representation of a date.
 */
public class Date {

  private int year;
  private int month;
  private int day;

  /**
   * Creates a date using the given values for year, month and day.
   * Throws an IllegalArgumentException if an invalid year, month or day is given.
   *
   * @param y Year
   * @param m Month
   * @param d Day
   */
  public Date(int y, int m, int d) {
    boolean valid = false;
    if (y<=0 || y>=10000) {
      throw new IllegalArgumentException("Not a valid year");
    }
    if (m<=0 || m>=13) {
      throw new IllegalArgumentException("Not a valid month");
    }
    if (d<=0 || d>=32) {
      throw new IllegalArgumentException("Not a valid day");
    }
    if (m==2 && d<=28) {
      valid = true;
    }
    if (m==4 || m==6 || m==9 || m==11 && d<=30) {
      valid = true;
    }
    if (m==1 || m==3 || m==5 || m==7 || m==8 || m==10 || m==12 && d<=31) { 
      valid = true;
    }
    if (valid) {
      year = y;
      month = m;
      day = d;
    }
  }

  /**
   * Returns the year component of this date.
   *
   * @return Year
   */
  public int getYear() {
    return year;
  }

  /**
   * Returns the month component of this date.
   *
   * @return Month
   */
  public int getMonth() {
    return month;
  }

  /**
   * Returns the day component of this date.
   *
   * @return Day
   */
  public int getDay() {
    return day;
  }

  /**
   * Returns the day of the year component of this date.
   * So January 1st would be 1 and December 31st would be 365.
   *
   * @return dayOfYear
   */
  public int getDayOfYear() {
    int dayOfYear = 0;

    if (month==1) {
      dayOfYear += 0;
    }
    if (month==2) {
      dayOfYear += 31;
    }
    if (month==3) {
      dayOfYear += 59;
    }
    if (month==4) {
      dayOfYear += 90;
    }
    if (month==5) {
      dayOfYear += 120;
    }
    if (month==6) {
      dayOfYear += 151;
    }
    if (month==7) {
      dayOfYear += 181;
    }
    if (month==8) {
      dayOfYear += 212;
    }
    if (month==9) {
      dayOfYear += 243;
    }
    if (month==10) {
      dayOfYear += 273;
    }
    if (month==11) {
      dayOfYear += 304;
    }
    if (month==12) {
      dayOfYear += 334;
    }

    dayOfYear += day;

    return dayOfYear;
  }

  /**
   * Provides a string representation of this date.
   *
   * ISO 8601 format is used (YYYY-MM-DD).
   *
   * @return Date as a string
   */
  @Override
  public String toString() {
    return String.format("%04d-%02d-%2d", year, month, day);
  }

  /**
   * Compares Date objects for equality of state.
   *
   * @param thing Object with which this Date is to be compared
   * @return True if the two dates are equal, false otherwise
   */
  @Override
  public boolean equals(Object thing) {

    if (thing == this) {
      return true;
    }
    else if (thing != null && thing instanceof Date) {
      Date thingDate = (Date) thing;
      return getYear() == thingDate.getYear()
          && getMonth() == thingDate.getMonth()
              && getDay() == thingDate.getDay();
      }
    else {
      return false;
    }
  }
}
