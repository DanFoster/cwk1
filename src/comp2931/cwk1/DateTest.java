package comp2931.cwk1;

import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class DateTest{
  @Test
  public void inputToString() {
      Date stringDate = new Date(1996, 6, 14);

      assertThat(stringDate.toString(), is("1996-06-14"));
  }

  @Test (expected = IllegalArgumentException.class)
  public void invalidYear() {
      new Date(0,9,10);
  }

  @Test (expected = IllegalArgumentException.class)
  public void invalidMonth() {
      new Date(1,13,10);
  }

  @Test (expected = IllegalArgumentException.class)
  public void invalidDay() {
      new Date(1,9,33);
  }

  @Test
  public void equality() {
    Date d = new Date(1996, 10, 1);

    assertThat(d.equals(d), is(true));
    assertThat(d.equals(new Date(1996, 10, 1)), is(true));
    assertThat(d.equals(new Date(1995, 10, 1)), is(false));
    assertThat(d.equals(new Date(1996, 11, 1)), is(false));
    assertThat(d.equals(new Date(1996, 10, 2)), is(false));
  }

  @Test
  public void getDayOfYear() {
      Date jan1 = new Date(1996, 1, 1);
      Date dec31 = new Date(1996, 12, 31);

      assertThat(jan1.getDayOfYear(), is(1));
      assertThat(dec31.getDayOfYear(), is(365));
  }

}